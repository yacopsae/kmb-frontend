import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from 'src/app/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {


  // FOR DOCKER
  // private baseUrl = 'http://192.168.100.23:8080/employers-service';

  // FOR LOCALHOST
  private baseUrl = 'http://localhost:8080/employers-service';

  constructor(private http: HttpClient) { }

  createEmployee(employee: Employee): Observable<any> {
    employee.recruitmentDate = new Date().toISOString();
    return this.http.post(`${this.baseUrl}/employee`, employee);
  }

  updateEmployee(employee: Employee): Observable<any> {
    this.http.put(`${this.baseUrl}/employee`, employee).subscribe(value => console.log(value));
    return this.http.put(`${this.baseUrl}/employee`, employee);
  }

  deleteEmployee(id: number): Observable<any>  {
    return this.http.delete(`${this.baseUrl}/employee/${id}`);
  }

  getEmployeesList(): Observable<any> {
    return this.http.get(`${this.baseUrl}/employees`);
  }
}
