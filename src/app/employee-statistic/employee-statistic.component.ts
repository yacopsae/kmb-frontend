import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AverageExperience } from 'src/app/average.experience';
import { StatisticService } from 'src/app/statistic.service';
import { TopPost } from 'src/app/top.post';

@Component({
  selector: 'app-employee-statistic',
  templateUrl: './employee-statistic.component.html',
  styleUrls: ['./employee-statistic.component.css']
})
export class EmployeeStatisticComponent implements OnInit {
  topPost: Observable<TopPost>;
  averageExperience: Observable<AverageExperience>;

  constructor(private statisticService: StatisticService) { }

  ngOnInit(): void {
    this.reloadData();
  }

  reloadData(): void {
    this.averageExperience = this.statisticService.getAverageExperience();
    this.topPost = this.statisticService.getTopPost();
  }

  getAverageExpirience(): Observable<any> {
    return this.statisticService.getAverageExperience();
  }

  getTopPost(): Observable<any> {
    return this.statisticService.getTopPost();
  }
}
