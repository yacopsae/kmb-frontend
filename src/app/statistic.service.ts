import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
@Injectable({
  providedIn: 'root'
})
export class StatisticService {

  // FOR DOCKER
  // private baseUrl = 'http://192.168.100.23:8080/employers-client';

  // for localhost
  private baseUrl = 'http://localhost:8080/employers-client';

  constructor(private http: HttpClient) {
  }

  getAverageExperience(): Observable<any> {
    return this.http.get(`${this.baseUrl}/averageWorkExperience`);
  }

  getTopPost(): Observable<any> {
    return this.http.get(`${this.baseUrl}/post/top`);
  }
}
