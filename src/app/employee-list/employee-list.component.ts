import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  employees: Observable<Employee[]>;

  constructor(private employeeService: EmployeeService) {}

  ngOnInit(): void {
    this.reloadData();
  }

  public reloadData(): void {
    this.employees = this.employeeService.getEmployeesList();
  }

  terminateEmployee(id: number, employee: Employee): void {
    employee.terminationDate = new Date().toISOString();
    this.employeeService.updateEmployee(employee);
    this.reloadData();
  }

  deleteEmployee(id: number): void {
    this.employeeService.deleteEmployee(id);
  }

}
