export class Employee {
  id: number;
  name: string;
  surname: string;
  secondName: string;
  post: string;
  recruitmentDate: string;
  terminationDate: string;
}
